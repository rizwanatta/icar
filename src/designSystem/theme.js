import { createTheme } from "@rneui/themed";

const theme = createTheme({
  lightColors: {
    primary: "red",
  },
  darkColors: {
    primary: "blue",
  },
  components: {
    Button: {
      outline: true,
      color: "orange",
      buttonStyle: {
        borderRadius: 10,
        padding: 15,
      },
      containerStyle: {
        margin: 5,
      },
    },
  },
});

export { theme };
